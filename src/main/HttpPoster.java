package main;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.X509TrustManager;

import ae.routines.S;

import com.google.gson.Gson;

public class HttpPoster {

    private URL url;
    private HttpURLConnection urlConn;
    private Gson gson;

    /**
     * @param url to which {@link #post(String)} sends data to
     */
    public HttpPoster(String url) throws MalformedURLException {
        this.url = new URL(url);
        gson = new Gson();
    }

    /**
     * open connection, send data with "Content-application/json", close connection
     * 
     * @param data - data for the post. json conversion using "com.google.gson"
     * @return String[0] server response eg. "200 OK",
     *         String[1] server response eg. "<!DOCTYPE..."
     * @throws Exception if an error occurred
     * @throws CertificateException if ssl certificate didn't match expected fingerprint
     */
    public String[] post(Object data) throws Exception {

        if ("https".equals(url.getProtocol())) {
            sslSetup();
        } else {
            urlConn = (HttpURLConnection) url.openConnection();
        }

        urlConn.setDoOutput(true);
        // urlConn.setInstanceFollowRedirects(false);
        urlConn.setUseCaches(false);
        urlConn.setRequestMethod("POST");
        urlConn.setRequestProperty("Content-Type", "application/json");
        urlConn.setRequestProperty("User-Agent", Utils.getProp("userAgent"));

        byte[] bytes = gson.toJson(data).getBytes(Utils.UTF8);
        urlConn.setRequestProperty("Content-Length", Integer.toString(bytes.length));
        urlConn.getOutputStream().write(bytes);
        urlConn.getOutputStream().flush();
        urlConn.getOutputStream().close();

        /*
         * for (Entry<String, List<String>> e :
         * urlConn.getHeaderFields().entrySet()) { S.printf("header: '%s': %s",
         * e.getKey(), e.getValue().toString()); }
         */

        StringBuilder sb = new StringBuilder();
        BufferedReader br = new BufferedReader(new InputStreamReader(urlConn.getInputStream(), Utils.UTF8));
        
        // TODO: replace with for-loop rather than unnecessary string re-lining with readLine()
        for (String line = br.readLine(); line != null; line = br.readLine()) {
            sb.append(line);
            sb.append("\n");
        }

        String result[] = {
                S.sprintf("%s %s", urlConn.getResponseCode(), urlConn.getResponseMessage()),
                sb.toString()
                };

        urlConn.getInputStream().close();
        urlConn.disconnect();

        return result;
    }

    private void sslSetup() throws Exception {
        urlConn = (HttpsURLConnection) url.openConnection();

        /* "SSLContext Algorithms"
         * http://docs.oracle.com/javase/7/docs/technotes/guides/security/StandardNames.html#SSLContext
         * hope that "TLS" SSLContext can do ssl & tls, all versions. would be
         * tedious to change here based on what config file
         */
        SSLContext sslCtx = SSLContext.getInstance("TLS");

        sslCtx.init(null, new X509TrustManager[] { new X509TrustManager() {

            @Override
            public X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[] {};
            }

            @Override
            public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                String fpGot = Utils.SHA1(chain[0].getEncoded());
                String fpExpected = Utils.getProp("httpsFingerprint");

                if (!fpExpected.equals(fpGot)) {
                    throw new CertificateException(
                            S.sprintf(
                                    "Connection to '%s' aborted. Expected fingerprint '%s', but was '%s'",
                                    urlConn.getURL().toString(), fpExpected,
                                    fpGot));
                }
            }

            @Override
            public void checkClientTrusted(X509Certificate[] chain,
                    String authType) throws CertificateException {
                throw new CertificateException(
                        "This X509TrustManager doesn't authenticate peers");
            }
        } }, null);

        ((HttpsURLConnection) urlConn).setSSLSocketFactory(sslCtx
                .getSocketFactory());

        /*
         * // prehaps hostname is originally verified by the overridden
         * X509TrustManager urlConn.setHostnameVerifier(new HostnameVerifier() {
         * 
         * @Override public boolean verify(String hostname, SSLSession session)
         * { // fingerprint checking suffices in our case return true; } });
         */

        /*
         * urlConn.setSSLSocketFactory(new
         * SSLSocketFactoryProxy(sslCtx.getSocketFactory()) {
         * 
         * @Override public void afterSocketCreated(SSLSocket socket) {
         * S.printf(Arrays.toString(socket.getSupportedProtocols()));
         * S.printf(Arrays.toString(socket.getEnabledCipherSuites()));
         * socket.setEnabledCipherSuites(new String[] {}); } });
         */
    }

}
