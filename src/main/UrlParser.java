package main;

import java.util.LinkedList;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * given text, this class fuzzily parses urls beginning with http://, https://, www., and ftp://
 */
public class UrlParser {

    private final static AtomicReference<UrlParser> instance = new AtomicReference<>();

    private final Pattern urlPrepP;
    private final Matcher urlPrepM;

    private final Pattern urlP;
    private final Matcher urlM;

    private final Pattern schemeP;
    private final Matcher schemeM;

    private final Pattern validCharsP;
    private final Matcher validCharsM;

    private final ThreadLocal<StringBuffer> urlBuilder;
    private final ThreadLocal<LinkedList<String>> resultUrls;

    /**
     * multithread-safe
     */
    public static UrlParser getInstance() {
        instance.compareAndSet(null, new UrlParser());
        return instance.get();
    }

    private UrlParser() {
        // for prepending space to an url that appears the be mistakenly joined
        // with previous text
        urlPrepP = Pattern.compile("(?<=[a-z])" + "(?<prep>" + "	(?:"
                + "		(?:http|https|ftp)://" + "		|www\\d{0,5}\\.[^\\.,\\s]"
                + "	)" + "	(?:.+?)" + ")", Pattern.COMMENTS
                | Pattern.CASE_INSENSITIVE);
        urlPrepM = urlPrepP.matcher("");

        // optimistic url matching
        urlP = Pattern
                .compile("(?:,\\s|\\s|\\x01|^)" + "	(?<url>" + "		(?:"
                        + "			(?:http|https|ftp)://" + "			|"
                        + "			www\\d{0,5}\\.[^\\.,\\s]" + "		)" + "		(?:.+?)"
                        + "	)" + "(?=,\\s|\\s|$)", Pattern.COMMENTS
                        | Pattern.CASE_INSENSITIVE);
        urlM = urlP.matcher("");

        // non-standardly checking whether url begins with scheme://
        schemeP = Pattern.compile("^[a-z]+://.*$", Pattern.CASE_INSENSITIVE);
        schemeM = schemeP.matcher("");

        // for replacing characters that are not supposed to appear as-is in an
        // url
        validCharsP = Pattern
                .compile("[^A-Za-z0-9-_.~!*'();:@&=+$,/?#\\[\\]%]+");
        validCharsM = validCharsP.matcher("");
        urlBuilder = new ThreadLocal<StringBuffer>() {
            @Override
            protected StringBuffer initialValue() {
                return new StringBuffer();
            }
        };

        resultUrls = new ThreadLocal<LinkedList<String>>() {
            @Override
            protected LinkedList<String> initialValue() {
                return new LinkedList<>();
            }
        };
    }

    /**
     * @return new instance
     */
    public LinkedList<String> parseText(String rawText) {
        resultUrls.get().clear();

        // prepare urls
        urlPrepM.reset(rawText);
        rawText = urlPrepM.replaceAll(" ${prep}");

        // match urls
        urlM.reset(rawText);
        while (urlM.find()) {
            // add found url to result after "standardifying" them
            resultUrls.get().add(standardify(urlM.group("url")));
        }

        return resultUrls.get();
    }

    private String standardify(String url) {
        // if [scheme]:// not specified, assume&append http
        schemeM.reset(url);
        if (!schemeM.matches())
            url = "http://" + url;

        // if contains invalid characters, replace them using percent-encoding
        validCharsM.reset(url);
        urlBuilder.get().setLength(0);

        while (validCharsM.find()) {
            // append from last match to before current match
            validCharsM.appendReplacement(urlBuilder.get(), "");

            // invalid bytes to percent-encoding (decoding to bytes, assuming
            // utf8)
            byte[] bytes = validCharsM.group().getBytes(Utils.UTF8);
            for (int i = 0; i < bytes.length; i++) { // classic multi-evaluating
                                                     // less-than-length..optimize?:C
                urlBuilder.get().append('%');
                urlBuilder.get().append(Integer.toString(bytes[i] & 0xff, 1 + 0xf));
            }
        }

        // append remaining, non-matching
        validCharsM.appendTail(urlBuilder.get());

        return urlBuilder.toString();
    }

}
