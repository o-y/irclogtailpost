package main;

import ifaces.IRCLogLineReceiver;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

import threads.PostThread;
import threads.TailThread;
import ae.routines.S;

public class Main implements IRCLogLineReceiver {

    public static void main(String[] args) {
        if (args.length == 2) {
            new Main().main(args[0], args[1]);
        } else {
            S.eprintf("args: <file to tail> [channel name to send]");
            System.exit(1);
        }
    }

    private TailThread tailThread;
    private final PostThread postThread;
    private ExecutorService es = Executors.newFixedThreadPool(2);
    private AtomicBoolean isShutdown = new AtomicBoolean(false);
    
    public Main() {
        postThread = new PostThread();
    }

    public void main(String filePath, String channelName) {
        tailThread = new TailThread(filePath, this);

        Utils.installTerminateHandler(new Runnable() {
            @Override
            public void run() {
                // runs inside an unknown thread
                Main.this.shutdown();
            }
        });

        // ExecutorService has 2 threads. tailer reserves one
        es.execute(tailThread);

        // http post thread reserves the other
        es.execute(postThread);

        // both threads are reserved. this will be called when either one is
        // freed (TailThread or PostThread returns)
        es.execute(new Runnable() {
            @Override
            public void run() {
                Main.this.shutdown();
            }
        });

    }

    protected void shutdown() {
        // runs inside an unknown thread

        if (!isShutdown.compareAndSet(false, true)) return;
        S.printf("\nQuitting..");

        tailThread.shutdown();
        postThread.shutdown();
    }

    @Override
    public void ircLogLineReceived(IRCLogLine parsedLine) {
        // called from an unknown thread
        postThread.addIRCLogLine(parsedLine);
    }

}
