package main;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.LinkedList;

/**
 * doesn't take into account file getting smaller or disappearing.
 */
public class Tail {

    private volatile boolean allowRun;
    private volatile Thread startThread = null;

    // methods

    /**
     * blocks until {@link #requestTerminate()} is called or internal error occurs,
     * in which case an exception is thrown.
     * {@link TailLineReceiver} callbacks blocks the rest of tail-reading until return and
     * are called in the thread running this function.
     */
    public void follow(String filePath, int sleepTime, TailLineReceiver r)
            throws Exception {
        allowRun = true;
        synchronized (startThread) {
            startThread = Thread.currentThread();
        }

        FileInputStream fis = null;
        BufferedReader br = null;
        LinkedList<String> lineBuffer = new LinkedList<>();

        try {
            fis = new FileInputStream(filePath);
            fis.getChannel().position(fis.getChannel().size());
            br = new BufferedReader(new InputStreamReader(fis, Utils.UTF8));

            while (allowRun) {
                try {
                    Thread.sleep(sleepTime);
                } catch (InterruptedException e) {}

                while (true) {
                    String read = br.readLine();
                    if (read != null) {
                        lineBuffer.add(read);
                    } else {
                        if (lineBuffer.size() > 0)
                            r.lineReceived(lineBuffer);
                        lineBuffer.clear();
                        break;
                    }
                }

            }
        } catch (Exception e) {
            if (e instanceof InterruptedException) { // if this even accomplishes anything
                return;
            } else {
                if (!allowRun) throw e;
            }
        } finally {
            try {
                br.close();
            } catch (Exception t) {}
            startThread = null;
        }
        
        /*
         * } catch (IOException e) { if (!allowRun) throw e; // else do nothing,
         * because we were asked to stop tailing } finally {
         * 
         * }
         */
    }

    /**
     * can be called from any thread
     */
    public void stop() {
        allowRun = false;
        synchronized (startThread) {
            if (startThread != null) {
                startThread.interrupt(); // if this even accomplishes anything
            }
        }
    }

    // classes -------------------------------------------

    public static interface TailLineReceiver {
        /**
         * called from: see {@link Tail#follow}
         * 
         * @param lines - used internally by {@link Tail#follow}. do not hold references after callback returns
         */
        void lineReceived(LinkedList<String> lines);
    }

}
