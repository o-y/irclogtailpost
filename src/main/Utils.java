package main;

import java.io.FileReader;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Properties;

import sun.misc.Signal;
import sun.misc.SignalHandler;

public class Utils {

    public static final Charset UTF8 = Charset.forName("UTF-8");

    /*
     * run cb when sigint or sigterm is caught, afterwards let normal signal handlers run (jvm shutdown hooks).
     * using non-standard sun.misc classes :(
     */
    public static void installTerminateHandler(final Runnable cb) {

        new Runnable() {
            SignalHandler sigHandler;
            SignalHandler oldSigIntHandler;
            SignalHandler oldSigTermHandler;
            SignalHandler sh;

            @Override
            public void run() {

                sigHandler = new SignalHandler() {
                    @Override
                    public void handle(Signal signal) {
                        switch (signal.getName()) {
                        case "INT":
                            sh = oldSigIntHandler;
                            break;
                        case "TERM":
                            sh = oldSigTermHandler;
                            break;
                        }
                        cb.run();
                        sh.handle(signal);
                    }
                };

                oldSigIntHandler = Signal.handle(new Signal("INT"), sigHandler);
                oldSigTermHandler = Signal.handle(new Signal("TERM"), sigHandler);

            }
        }.run();
    }

    public static String SHA1(byte[] bytes) {
        try {
            // http://docs.oracle.com/javase/7/docs/technotes/guides/security/StandardNames.html#MessageDigest
            MessageDigest md = MessageDigest.getInstance("SHA-1");
            md.update(bytes);
            bytes = md.digest();

            return String.format("%040x", new BigInteger(1, bytes));
        } catch (NoSuchAlgorithmException e) {
            throw new Error("SHA-1 algoritm not found", e);
        }
    }

    private static Properties props;
    static {
        props = new Properties();
        try {
            props.load(new FileReader("main.properties"));
        } catch (IOException e) {
            throw new Error("Failed to load program settings (main.properties)", e);
        }
    }

    public static String getProp(String key) {
        String ret = (String) props.get(key);
        if (ret == null) return "";
        else return ret;
    }

    public static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {}
    }

}
