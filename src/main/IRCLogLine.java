package main;

import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ae.routines.S;

public class IRCLogLine {

    public final Date date;
    public final String nick;
    public final LinkedList<String> urls;

    private IRCLogLine(Date d, String n, LinkedList<String> u) {
        date = d;
        nick = n;
        urls = u;
    }

    
    private static Pattern lineP;
    private static Matcher lineM;
    
    {
        // 9999-12-31 24:59:59 +0000       <nick>
        lineP = Pattern.compile("^(?<year>[0-9]{4})-(?<month>[0-9]{2})-(?<day>[0-9]{2})[ ]" +
        		                "(?<hour>[0-9]{2}):(?<minute>[0-9]{2}):(?<second>[0-9]{2})[ ]" +
        		                "(?<timezone>[+-][0-9]{4})\\s+" +
        		                "<(?<nick>[^>]+)>", Pattern.COMMENTS);
        lineM = lineP.matcher("");
    }
    
    /**
     * @param rawText - one irc log line
     * @return null if failed
     */
    public static IRCLogLine parseLine(String rawText) {

        lineM.reset(rawText);
        if (!lineM.matches()) return null;
        
        // do parse
        
        TimeZone tz = TimeZone.getTimeZone(S.sprintf("GMT%s", lineM.group("timezone")));
        Calendar cal = Calendar.getInstance(tz);
        
        cal.set(Integer.parseInt(lineM.group("year")),
                Integer.parseInt(lineM.group("month")),
                Integer.parseInt(lineM.group("day")),
                Integer.parseInt(lineM.group("hour")),
                Integer.parseInt(lineM.group("minute")),
                Integer.parseInt(lineM.group("second"))
                );
        
        LinkedList<String> urls = new LinkedList<>();
        for (String s : UrlParser.getInstance().parseText(rawText)) {
            // printf("- " + s);
            urls.add(s);
        }
        return new IRCLogLine(cal.getTime(), lineM.group("nick"), urls);
    }
}
