package main;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

public class SSLSocketFactoryProxy extends SSLSocketFactory {

    private SSLSocketFactory parent;
    private int callDepth;

    /**
     * @param the instance to which non-overridden calls to this instance will be forwarded to
     */
    public SSLSocketFactoryProxy(SSLSocketFactory parent) {
        /*
         * "Obtaining an SSLSocket"
         * http://docs.oracle.com/javase/7/docs/technotes/guides/security/jsse/JSSERefGuide.html#SSLSocketFactory
         */
        
        this.parent = parent;
        callDepth = 0;
    }

    /**
     * do here what you want with the newly-created {@link SSLSocket}. called only once. this implementation does nothing
     */
    public void afterSocketCreated(SSLSocket socket) {
    }

    @Override
    public String[] getDefaultCipherSuites() {
        return parent.getDefaultCipherSuites();
    }

    @Override
    public String[] getSupportedCipherSuites() {
        return parent.getSupportedCipherSuites();
    }

    @Override
    public Socket createSocket(Socket s, String host, int port, boolean autoClose) throws IOException {
        callDepth++;
        Socket newSocket = parent.createSocket(s, host, port, autoClose);
        depthCheck(newSocket);
        return newSocket;
    }

    @Override
    public Socket createSocket(String host, int port) throws IOException, UnknownHostException {
        callDepth++;
        Socket newSocket = parent.createSocket(host, port);
        depthCheck(newSocket);
        return newSocket;
    }

    @Override
    public Socket createSocket(InetAddress host, int port) throws IOException {
        callDepth++;
        Socket newSocket = parent.createSocket(host, port);
        depthCheck(newSocket);
        return newSocket;
    }

    @Override
    public Socket createSocket(String host, int port, InetAddress localHost, int localPort) throws IOException, UnknownHostException {
        callDepth++;
        Socket newSocket = parent.createSocket(host, port, localHost, localPort);
        depthCheck(newSocket);
        return newSocket;
    }

    @Override
    public Socket createSocket(InetAddress address, int port, InetAddress localAddress, int localPort) throws IOException {
        callDepth++;
        Socket newSocket = parent.createSocket(address, port, localAddress, localPort);
        depthCheck(newSocket);
        return newSocket;
    }

    private void depthCheck(Socket newSocket) {
        callDepth--;
        if (callDepth == 0) {
            afterSocketCreated((SSLSocket) newSocket);
        }
    }

}
