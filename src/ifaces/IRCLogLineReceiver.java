package ifaces;

import main.IRCLogLine;

public interface IRCLogLineReceiver {
    /**
     * called from an unknown thread
     * @param parsedLine - all fields are final: content visible to all threads
     */
    void ircLogLineReceived(IRCLogLine parsedLine);
}
