package threads;

import java.util.concurrent.ConcurrentLinkedQueue;

import main.HttpPoster;
import main.IRCLogLine;
import main.Utils;
import ae.routines.S;

import com.google.gson.Gson;

/**
 * collects irc urls and posts them according to settings
 */
public class PostThread implements Runnable {
    
    private final ConcurrentLinkedQueue<IRCLogLine> logLines;
    private volatile boolean allowRun = true;
    private long lastPostTime = 0;
    
    public PostThread() {
        logLines = new ConcurrentLinkedQueue<>();
    }

    @Override
	public void run() {
		S.printf("[HTTP/Post Thread] Start");
		
		HttpPoster poster;
		
		try {
		    poster = new HttpPoster(Utils.getProp("url"));
		    while (allowRun) {
		        loop(poster);
		    }
		} catch (Exception e) {
			new Exception("HTTP/Post thread failed", e).printStackTrace();
		}
		
		S.printf("[HTTP/Post Thread] Stop");
	}

    private void loop(HttpPoster poster) {
        
        if (logLines.size() < Long.parseLong(Utils.getProp("postWhenLines")) &&
            System.currentTimeMillis() - lastPostTime < Long.parseLong(Utils.getProp("postWhenTime")))
        {
            try { Thread.sleep(1000); } catch (InterruptedException e) {}
            return;
        }
        
        // do post
        
        // poster.post(logLines);
        System.out.println("[Post Thread] POST: " + new Gson().toJson(logLines));
        logLines.clear();
        lastPostTime = System.currentTimeMillis();
    }

    public void addIRCLogLine(IRCLogLine parsedLine) {
        logLines.add(parsedLine);
    }

    public void shutdown() {
        // TODO Auto-generated method stub
        
    }
}
