package threads;

import static ae.routines.S.printf;
import ifaces.IRCLogLineReceiver;

import java.util.LinkedList;

import main.IRCLogLine;
import main.Tail;
import main.Tail.TailLineReceiver;

public class TailThread implements Runnable, TailLineReceiver {

    private final Tail tail;
    private final String filePath;
    private final IRCLogLineReceiver listener;

    public TailThread(String filePath, IRCLogLineReceiver listener) {
        tail = new Tail();
        this.filePath = filePath;
        this.listener = listener;
    }

    @Override
    public void run() {
        printf("[Tail Thread] Start");

        try {
            tail.follow(filePath, 3000, this);
        } catch (Exception e) {
            new Exception("Tail Thread failed", e).printStackTrace();
        }

        printf("[Tail Thread] Stop");
    }

    @Override
    public void lineReceived(LinkedList<String> lines) {
        for (String l : lines) {
            listener.ircLogLineReceived(IRCLogLine.parseLine(l));
        }
    }

    public void shutdown() {
        tail.stop();
    }

}
